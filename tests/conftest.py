import os
import sys

import pytest

pytest_plugins = [
    'aiohttp.pytest_plugin',
]

my_path = os.path.dirname(os.path.abspath(__file__))  # pylint: disable=invalid-name
sys.path.insert(0, my_path + '/../')


@pytest.fixture
async def client(aiohttp_client, loop):  # pylint: disable=unused-argument
    from app import get_app  # pylint: disable=import-outside-toplevel

    app = get_app()
    client = await aiohttp_client(app)

    yield client
