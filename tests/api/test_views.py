import validx


class TestHoroscopeApi:
    validate_response_get = validx.Dict({
        'result': validx.Str(minlen=1)
    })

    async def test_get_horoscope(self, client):
        resp = await client.get('/api/horoscope', params={'zodiac': 'Aries', 'dt': '15/01/2021'})

        assert resp.status == 200
        data = await resp.json()
        self.validate_response_get(data)
