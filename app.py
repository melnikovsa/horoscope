#!/usr/bin/env python3

from aiohttp import web

from horoscope.api import middlewares
from horoscope.api import urls


def get_app():
    app = web.Application(middlewares=[middlewares.errors])
    app.add_routes(urls.routes)
    return app


def run_web():
    web.run_app(
        app=get_app(),
        host='0.0.0.0',
        port=8000,
    )


if __name__ == "__main__":
    run_web()
