## Installation
### Requirements:
```bash
pip3 install -r requirements,txt
```


## Configuration
The environment config example placed into `.env.example` file.

First of all, when you clone the repo you should initialize database:
```bash
cp .env.example .env
```

## Testing
Run local (isolated) tests
```
pytest
```

## Run Server
```
python3 app.py
```


## Examples

http://127.0.0.1:8000/api/horoscope?zodiac=Aries&dt=15/01/2021