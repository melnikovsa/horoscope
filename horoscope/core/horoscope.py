import functools

import aiohttp

from horoscope import settings
from horoscope.core import helpers


@functools.lru_cache(maxsize=128, typed=False)
@helpers.cacheable
async def get_horoscope(zodiac: str, dt: str) -> str:
    async with aiohttp.ClientSession(raise_for_status=True) as session:
        resp = await session.get(
            settings.API_URL,
            params={
                'zodiac': settings.ZODIAC_MAPPING[zodiac],
                'show_same': 'true',
                'type': 'big',
                'date': dt,
                'api_key': settings.API_KEY
            }
        )
        data = await resp.json()
        if data['status'] == 200:
            return data['response']['bot_response']

        return data['response']
