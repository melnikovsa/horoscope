from aiohttp import web

from . import views

routes = [
    web.view('/api/horoscope', views.HoroscopeView),
]
