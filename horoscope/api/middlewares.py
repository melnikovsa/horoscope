import json

import validx
from aiohttp import web


@web.middleware
async def errors(request, handler):
    try:
        response = await handler(request)
    except validx.exc.ValidationError as exc:
        return web.Response(body=json.dumps({'error': str(exc)}),
                            status=400,
                            content_type='application/json')
    return response
