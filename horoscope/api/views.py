import aiohttp
import validx
from aiohttp import web_response

from horoscope import settings
from horoscope.core import horoscope


class HoroscopeView(aiohttp.web.View):
    get_query_string = validx.Dict({
        'zodiac': validx.Str(options=settings.ZODIAC_MAPPING.keys()),
        'dt': validx.Date(format='%d/%m/%Y')
    }, optional=['currency'])

    async def get(self):
        data = self.request.query
        self.get_query_string(data)
        return web_response.json_response({'result': await horoscope.get_horoscope(
            zodiac=data['zodiac'],
            dt=data['dt'],
        )})
