import os

from dotenv import load_dotenv

load_dotenv()

API_KEY = os.environ['API_KEY']
API_URL = f'https://api.vedicastroapi.com/json/prediction/daily'

ZODIAC_MAPPING = {
    'Aries': 1,
    'Taurus': 2,
    'Gemini': 3,
    'Cancer': 4,
    'Leo': 5,
    'Virgo': 6,
    'Libra': 7,
    'Scorpio': 8,
    'Sagittarius': 9,
    'Capricorn': 10,
    'Aquarius': 11,
    'Pisces': 12
}
